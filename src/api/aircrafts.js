// import request from "./request";
import aircrafts from './mockData/aircrafts.json';

export function getAircrafts({ offset, limit }) {
  // return request(`/aircrafts?offset=${offset}&limit=${limit}`);
  return Promise.resolve(aircrafts);
}

export function getAircraft(id) {
  if (!id) return null;

  // return request(`/aircrafts/${id}`);
  return Promise.resolve(
    aircrafts.data.find(({ id: aircraftId }) => id === aircraftId)
  );
}
