// import request from "./request";
import flights from './mockData/flights.json';

export function getFlights({ offset, limit }) {
  // return request(`/flights?offset=${offset}?limit=${limit}`);
  return Promise.resolve(flights);
}

export function getFlight(id) {
  if (!id) return null;

  // return request(`/flights/${id}`);
  return Promise.resolve(
    flights.data.find(({ id: flightId }) => id === flightId)
  );
}
