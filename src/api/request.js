async function request(url, options = {}) {
  const path = `${process.env.REACT_APP_API_BASE_URL}${url}`;
  let res = null;

  try {
    res = await fetch(`${path}`, {
      headers: {
        'Content-Type': 'application/json',
        ...options.headers
      },
      ...options,
    });
    const result = await res.json();

    if (!res.ok) {
      throw result;
    }
    return result;
  } catch (error) {
    error.errors = error.errors || {};
    throw error;
  }
}

export default request;
