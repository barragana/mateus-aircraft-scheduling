import React from 'react';
import styled from 'styled-components';
import { withFontSize } from '../styles/utils';
import Draggable from './Draggable';

const Header = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  height: 3rem;
  width: 100%;
  ${withFontSize};
`;

const DetailsContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const DetailsWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Text = styled.span`
  align-items: center;
  display: flex;
  justify-content: center;
  ${withFontSize};
`;

const FlightCard = ({
  id,
  readableDeparture,
  readableArrival,
  origin,
  destination,
  onMouseDown,
}) => (
  <Draggable onMouseDown={onMouseDown} draggable>
    <Header size="m">{id}</Header>
    <DetailsContainer>
      <DetailsWrapper>
        <Text size="m">{origin}</Text>
        <Text size="m">({readableDeparture})</Text>
      </DetailsWrapper>
      <DetailsWrapper>
        <Text size="m">{destination}</Text>
        <Text size="m">({readableArrival})</Text>
      </DetailsWrapper>
    </DetailsContainer>
  </Draggable>
);

export default FlightCard;
