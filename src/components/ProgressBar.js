import React from 'react';
import styled from 'styled-components';
import { SCHEDULES } from '../business/rules';
import { withFontSize } from '../styles/utils';

const OuterContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: ${({ theme }) => theme.spacing.xxl};
  padding-bottom: ${({ theme }) => theme.spacing.m};
  width: 100%;
`;

const Container = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
`;

const LabelsContainer = styled.div`
  display: flex;
  margin-bottom: ${({ theme }) => theme.spacing.xxs};
`;

const Line = styled.div`
  background-color: black;
  margin: 0.175rem;
  height: 0.175rem;
  position: relative;
  width: 100%;
`;

const Marker = styled.div`
  background-color: black;
  height: 0.5rem;
  position: relative;
  width: 0.175rem;
`;

const Time = styled.div`
  font-weight: bold;
  left: ${({ left }) => `${left}px`};
  position: absolute;
  top: -18px;
  ${withFontSize}
`;

const SCHEDULES_COLORS = {
  [SCHEDULES.service]: 'green',
  [SCHEDULES.turnaround]: 'orange',
  [SCHEDULES.idle]: 'grey',
}

const Bar = styled.div`
  background-color: ${({ scheduled }) => SCHEDULES_COLORS[scheduled]};
  border: 1px solid white;
  height: 100%;
  width: ${({ width }) => `${width}%`};

  &:first-child {
    margin-left: 0;
  }
  &:last-child {
    margin-right: 0;
  }
`;

const ProgressBase = ({ timeline }) => {
  return (
    <OuterContainer>
      <LabelsContainer>
        <Marker>
          <Time size="xs" left={-17}>00:00</Time>
        </Marker>
        <Line />
        <Marker>
          <Time size="xs" left={-16}>12:00</Time>
        </Marker>
        <Line />
        <Marker>
          <Time size="xs" left={-17}>23:59</Time>
        </Marker>
      </LabelsContainer>
      <Container>
        {timeline.map(({ scheduled, size }, i) => (
          <Bar key={`${size}_${scheduled}_${i}`} width={size} scheduled={scheduled} />
        ))}
      </Container>
    </OuterContainer>
  );
};

export default ProgressBase;
