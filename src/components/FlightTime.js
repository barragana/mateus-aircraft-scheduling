import React from 'react';
import styled from 'styled-components';

import { withFontSize } from '../styles/utils';

const Container = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Details = styled.div`
  display: flex;
  flex-direction: column;
`;

const Text = styled.span`
  align-items: center;
  display: flex;
  justify-content: center;
  ${withFontSize};
`;

const ArrowSvg = () => (
  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 26.775 26.775" height="50">
  <g transform="translate(25, 0) rotate(90)">
    <path fill="#030104" d="M13.915,0.379l8.258,9.98c0,0,1.252,1.184-0.106,1.184c-1.363,0-4.653,0-4.653,0s0,0.801,0,2.025
      c0,3.514,0,9.9,0,12.498c0,0,0.184,0.709-0.885,0.709c-1.072,0-5.783,0-6.55,0c-0.765,0-0.749-0.592-0.749-0.592
      c0-2.531,0-9.133,0-12.527c0-1.102,0-1.816,0-1.816s-2.637,0-4.297,0c-1.654,0-0.408-1.24-0.408-1.24s7.025-9.325,8.001-10.305
      C13.24-0.414,13.915,0.379,13.915,0.379z"/>
  </g>
  </svg>
)

const FlightTime = ({ origin, departure, destination, arrival, direction }) => (
  <Container>
    <Details>
      <Text size="m">{origin}</Text>
      <Text size="m">({departure})</Text>
    </Details>
    {direction && (
      <ArrowSvg />
    )}
    <Details>
      <Text size="m">{destination}</Text>
      <Text size="m">({arrival})</Text>
    </Details>
  </Container>
);

export default FlightTime;
