import React from 'react';
import styled from 'styled-components';
import { withFontSize } from '../styles/utils';

const Container = styled.div`
  align-items: center;
  border-bottom: 1px solid ${({ theme }) => theme.colors.n4};
  cursor: pointer;
  display: flex;
  flex-flow: column;
  justify-content: center;
  padding: ${({ theme }) => theme.spacing.xl} ${({ theme }) => theme.spacing.m};
  width: 100%;
`;

const Text = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  width: 100%;
  ${withFontSize};
`;

const AircraftCard = ({ ident, usage, onClick }) => (
  <Container onClick={onClick}>
    <Text size="m">{ident}</Text>
    <Text size="m">({usage})</Text>
  </Container>
);

export default AircraftCard;
