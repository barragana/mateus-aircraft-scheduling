import styled, { css } from 'styled-components';

export const SidebarContainer = styled.div`
  display: grid;
  grid-area: ${({ gridArea }) => gridArea};
  grid-template-rows: 4rem auto;
  height: 100%;
  overflow-y: auto;
`;

const withBorderSide = ({ side, theme }) =>
  (side === 'left' && css`
    border-right: 1px solid ${theme.colors.n4};
  `) ||
  (side === 'right' && css`
    border-left: 1px solid ${theme.colors.n4};
  `);

export const SidebarScrollable = styled.div`
  border-top: 1px solid ${({ theme }) => theme.colors.n4};
  height: 100%;
  overflow-y: auto;
  ${withBorderSide};
`;

export const SidebarHeader = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
`;
