import styled from 'styled-components';

const VerticalCentered = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: center;
  padding: ${({ theme }) =>  theme.spacing.m};
`;

export default VerticalCentered;
