import React from 'react';
import styled from 'styled-components';

import { withFontSize } from '../styles/utils';
import FlightTime from './FlightTime';
import Draggable from './Draggable';

const Header = styled.div`
  align-items: center;
  display: flex;
  height: 3rem;
  justify-content: space-between;
  width: 100%;
  ${withFontSize};
`;

const Remove = styled.span`
  color: red;
  ${withFontSize}
`;

const RotationDrag = styled(Draggable)`
  padding-top: 0;
  margin-top: -0.25rem;
`;

const RotationCard = ({
  id,
  origin,
  departure,
  destination,
  arrival,
  onMouseDown,
  onRemove
}) => (
  <RotationDrag onMouseDown={onMouseDown}>
    <Header size="m">
      <span>Flight: {id}</span>
      {onRemove && (
        <Remove size="xs" onClick={onRemove}>Remove</Remove>
      )}
    </Header>
    <FlightTime
      origin={origin}
      departure={departure}
      destination={destination}
      arrival={arrival}
      direction
    />
  </RotationDrag>
);

export default RotationCard;
