import styled from 'styled-components';

const Draggable = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.n4};
  cursor: pointer;
  display: flex;
  flex-flow: column;
  justify-content: center;
  padding: ${({ theme }) => theme.spacing.xl} ${({ theme }) => theme.spacing.m};
  width: 100%;

  &:active {
    background-color: ${({ theme }) => theme.colors.n3};
    opacity: 1;
  }
`;

export default Draggable;
