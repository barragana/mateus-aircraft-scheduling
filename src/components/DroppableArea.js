import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import { withFontSize } from '../styles/utils';

const withoutPointerEvent = ({ show }) => !show && css`
  pointer-events: none;
`;

const withBGColor = ({ show, dark }) =>
  (show && ((dark && 'darkgreen') || 'lightgreen')) || 'transparent';

const Container = styled.div`
  background-color: ${withBGColor};
  height: 1.25rem;
  width: 100%;
  ${withoutPointerEvent};
`;

const Text = styled.span`
  align-items: center;
  display: flex;
  justify-content: center;
  ${withFontSize};
  color: white;
`;

const DroppableArea = ({ show, onDrop }) => {
  const [dark, setDark] = useState(false);

  return (
    <Container
      show={show}
      className="dropzone"
      dark={dark}
      onDragEnter={() => setDark(true)}
      onDragLeave={() => setDark(false)}
      onDrop={() => {
        onDrop();
        setDark(false);
      }}
    >
      <Text size="xs">Drop here</Text>
    </Container>
  )
}

export default DroppableArea;
