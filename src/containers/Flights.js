/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { connect } from 'react-redux';

import { fetchFlightsRequested } from '../ducks/flights';
import VerticalCentered from '../components/VerticalCentered';
import FlightCard from '../components/FlightCard';
import { SidebarContainer, SidebarHeader, SidebarScrollable } from '../components/Sidebar';
import { selectFlight } from '../ducks/rotation';

const ScrollableFlights = ({
  isFetching,
  didInvalidate,
  flights,
  selectFlight,
  rotationFlights,
}) => {
  if (didInvalidate) {
    return (
      <VerticalCentered>
        Something went wrong.
        <br/>
        Please, try refresh the page.
      </VerticalCentered>
    )
  }

  if (isFetching) {
    return (
      <VerticalCentered>
        Loading...
      </VerticalCentered>
    )
  }

  return (
    <SidebarScrollable side="right">
      {flights.map(({
        id,
        readable_departure,
        readable_arrival,
        origin,
        destination,
        ...attrs
      }) => !rotationFlights[id] && (
        <FlightCard
          key={id}
          id={id}
          readableDeparture={readable_departure}
          readableArrival={readable_arrival}
          origin={origin}
          destination={destination}
          onMouseDown={() => selectFlight({
            id,
            readable_departure,
            readable_arrival,
            origin,
            destination,
            ...attrs,
          })}
        />
      ))}
    </SidebarScrollable>
  );
};

const Flights = ({
  isFetching,
  didInvalidate,
  flights,
  offset,
  requestFlights,
  selectFlight,
  rotationFlights,
}) => {
  return (
    <SidebarContainer gridArea="flights">
      <SidebarHeader>Flights</SidebarHeader>
      <ScrollableFlights
        isFetching={isFetching}
        didInvalidate={didInvalidate}
        flights={flights}
        selectFlight={selectFlight}
        rotationFlights={rotationFlights}
      />
    </SidebarContainer>
  );
};

const mapStateToProps = state => ({
  ...state.flights.request,
  flights: state.flights.data,
  offset: state.flights.pagination?.offset,
  rotationFlights: state.rotation.flightsHash,
});

const mapDispatchToProps = {
  requestFlights: fetchFlightsRequested,
  selectFlight,
};

export default connect(mapStateToProps, mapDispatchToProps)(Flights);
