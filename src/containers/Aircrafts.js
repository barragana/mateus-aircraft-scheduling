/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { fetchAircraftsRequested } from '../ducks/aircrafts';
import AircraftCard from '../components/AircraftCard';
import VerticalCentered from '../components/VerticalCentered';
import { SidebarContainer, SidebarHeader, SidebarScrollable } from '../components/Sidebar';
import { addAircraft } from '../ducks/rotation';
import { calculateServiceUsage } from '../business/rules';

const ScrollableAircrafts = ({
  isFetching,
  didInvalidate,
  aircrafts,
  addAircraft,
  usage,
}) => {
  if (didInvalidate) {
    return (
      <VerticalCentered>
        Something went wrong.
        <br/>
        Please, try refresh the page.
      </VerticalCentered>
    )
  }

  if (isFetching) {
    return (
      <VerticalCentered>
        Loading...
      </VerticalCentered>
    )
  }

  return (
    <SidebarScrollable side="left">
      {aircrafts.map((aircraft) => (
        <AircraftCard
          key={aircraft.ident}
          ident={aircraft.ident}
          usage={`${Math.round(usage)}%`}
          onClick={() => { addAircraft(aircraft) }}
        />
      ))}
    </SidebarScrollable>
  );
};


const Aircrafts = ({
  isFetching,
  didInvalidate,
  aircrafts,
  offset,
  requestAircrafts,
  addAircraft,
  usage,
}) => {
  useEffect(() => {
    requestAircrafts();
  }, []);

  useEffect(() => {
    if(aircrafts.length) {
      addAircraft(aircrafts[0]);
    }
  }, [aircrafts]);
  return (
    <SidebarContainer gridArea="aircrafts">
      <SidebarHeader>Aircrafts</SidebarHeader>
      <ScrollableAircrafts
        isFetching={isFetching}
        didInvalidate={didInvalidate}
        aircrafts={aircrafts}
        addAircraft={addAircraft}
        usage={usage}
      />
    </SidebarContainer>
  );
};

const mapStateToProps = state => ({
  ...state.aircrafts.request,
  aircrafts: state.aircrafts.data,
  offset: state.aircrafts.pagination?.offset,
  usage: calculateServiceUsage(state.rotation.timeline),
});

const mapDispatchToProps = {
  requestAircrafts: fetchAircraftsRequested,
  addAircraft: addAircraft
};

export default connect(mapStateToProps, mapDispatchToProps)(Aircrafts);
