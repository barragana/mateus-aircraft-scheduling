/* eslint-disable react-hooks/exhaustive-deps */
import React, { useRef, useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { addFlight, removeFlight } from '../ducks/rotation';
import { canIncludesFlightAfter, canIncludesFlightIntoSpot } from '../business/rules';

import { SidebarContainer, SidebarHeader, SidebarScrollable } from '../components/Sidebar';
import DroppableArea from '../components/DroppableArea';
import RotationCard from '../components/RotationCard';
import ProgressBar from '../components/ProgressBar';

const ScrollableRotation = ({
  flights,
  onDrop,
  showDrop,
  onRemove,
  dragFlight,
}) => {
  const allowDropBefore = (index) => {
    const after = flights?.[index];
    const previous = flights?.[index - 1];
    return showDrop && canIncludesFlightIntoSpot(dragFlight, previous, after);
  }

  const allowDropAfter = showDrop
    && canIncludesFlightAfter(dragFlight, flights[flights.length - 1]);

  const handleRemove = (id, index) => {
    if (index > 0 && index < (flights.length - 1)) {
      return;
    }
    return () => onRemove(id);
  }
  return (
    <SidebarScrollable
      side="middle"
    >
      {flights.map(({
        id,
        readable_departure,
        readable_arrival,
        origin,
        destination,
      }, index) => (
        <React.Fragment key={id}>
          <DroppableArea
            show={allowDropBefore(index)}
            onDrop={() => onDrop(index)} />
          <RotationCard
            id={id}
            departure={readable_departure}
            arrival={readable_arrival}
            origin={origin}
            destination={destination}
            onRemove={handleRemove(id, index)}
          />
        </React.Fragment>
      ))}
      <DroppableArea key="0_drop" show={allowDropAfter} onDrop={() => onDrop(flights.length)} />
    </SidebarScrollable>
  );
};

const RotationContainer = styled(SidebarContainer)`
  grid-template-rows: 4rem auto 5.5rem;
`;

const Flights = ({
  flights,
  aircraftIdent,
  addFlight,
  removeFlight,
  dragFlight,
  timeline,
}) => {
  const ref = useRef(null);
  const [showDrop, setShowDrop] = useState(false);
  const handleDrop = (index) => {
    addFlight(index);
    setShowDrop(false);
  };

  const handleDragLeave = (e) => {
    const el = ref.current;
    // Using 2 as a threshold for border limit
    const left = el.offsetLeft + 2;
    const right = el.offsetLeft + el.clientWidth - 2;
    const top = el.offsetTop + 2;
    if(e.pageX < left || e.pageX > right || e.pageY < top) {
      setShowDrop(false);
    }
  };

  return (
    <RotationContainer
      gridArea="rotation"
      onDragEnter={() => {
        setShowDrop(true);
      }}
      onDragOver={(e) => {
        e.stopPropagation();
        e.preventDefault();
      }}
      onDragLeave={handleDragLeave}
      onDrop={() => {
        setShowDrop(false);
      }}
      ref={ref}
    >
      <SidebarHeader>Rotation {aircraftIdent}</SidebarHeader>
      <ScrollableRotation
        flights={flights}
        showDrop={showDrop}
        onDrop={handleDrop}
        onRemove={removeFlight}
        dragFlight={dragFlight}
      />
      <ProgressBar timeline={timeline} />
    </RotationContainer>
  );
};

const mapStateToProps = state => ({
  flights: state.rotation.flights,
  aircraftIdent: state.rotation.aircraft?.ident,
  dragFlight: state.rotation.selected,
  timeline: state.rotation.timeline,
});

const mapDispatchToProps = {
  addFlight,
  removeFlight,
};

export default connect(mapStateToProps, mapDispatchToProps)(Flights);
