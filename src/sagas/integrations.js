import { all, takeLatest, put } from 'redux-saga/effects';

import { fetchFlightsRequested } from '../ducks/flights';
import { ADD_AIRCRAFT } from '../ducks/rotation';

export function* handleSideEffectOfAircraftSelection({
  payload: { aircraft },
}) {
  yield put(fetchFlightsRequested(aircraft.ident));
};

export default function* watchData() {
  yield all([takeLatest(ADD_AIRCRAFT, handleSideEffectOfAircraftSelection)]);
}
