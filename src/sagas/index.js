import { all, fork } from 'redux-saga/effects';

import aircrafts from './aircrafts';
import flights from './flights';
import integrations from './integrations';

export default function* rootSaga() {
  yield all([
    fork(aircrafts),
    fork(flights),
    fork(integrations),
  ]);
}
