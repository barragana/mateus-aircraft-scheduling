import { all, takeLatest, put, call } from 'redux-saga/effects';

import { getFlights } from '../api/flights';
import { fetchFlightsFailed, fetchFlightsSuccessed, FETCH_FLIGHTS_REQUESTED } from '../ducks/flights';

export function* handleFetchFlights({ payload: { id, offset, limit }}) {
  try {
    const response = yield call(getFlights, { id, offset, limit });
    yield put(fetchFlightsSuccessed(response));
  } catch (e) {
    yield put(fetchFlightsFailed(e.errors));
  }
};

export default function* watchData() {
  yield all([takeLatest(FETCH_FLIGHTS_REQUESTED, handleFetchFlights)]);
}
