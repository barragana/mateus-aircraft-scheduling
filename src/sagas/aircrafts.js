import { all, takeLatest, put, call } from 'redux-saga/effects';

import { getAircrafts } from '../api/aircrafts';
import { fetchAircraftsFailed, fetchAircraftsSuccessed, FETCH_AIRCRAFTS_REQUESTED } from '../ducks/aircrafts';

export function* handleFetchAircrafts({ payload: { offset, limit }}) {
  try {
    const response = yield call(getAircrafts, { offset, limit });
    yield put(fetchAircraftsSuccessed(response));
  } catch (e) {
    yield put(fetchAircraftsFailed(e.errors));
  }
};

export default function* watchData() {
  yield all([
    takeLatest(FETCH_AIRCRAFTS_REQUESTED, handleFetchAircrafts),
  ]);
}
