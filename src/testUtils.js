import { render as rtlRender } from '@testing-library/react'
import { ThemeProvider } from 'styled-components';
import '@testing-library/jest-dom';

import theme, { GlobalStyle } from './styles/theme';

export const renderWithTheme = component => rtlRender(
  <ThemeProvider theme={theme}>
    <GlobalStyle />
    {component}
  </ThemeProvider>
);
export * from '@testing-library/react'
