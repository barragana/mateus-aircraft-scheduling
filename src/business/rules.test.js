import { canIncludesFlightAfter, canIncludesFlightBefore, buildScheduleTimelineService, SCHEDULES } from './rules';

describe('Business rules', () => {
  const rotationFlights = [
    {
      id: "AS1001",
      readable_departure: "06:00",
      readable_arrival: "07:15",
      origin: "LFSB",
      destination: "LFMN",
      departuretime: 21600,
      arrivaltime: 26100
    },
    {
      id: "AS1002",
      readable_departure: "07:45",
      readable_arrival: "08:55",
      origin: "LFMN",
      destination: "LFSB",
      departuretime: 27900,
      arrivaltime: 32100
    },
    {
      id: "AS1027",
      readable_departure: "09:45",
      readable_arrival: "11:15",
      origin: "LFSB",
      destination: "EDDH",
      departuretime: 35100,
      arrivaltime: 40500
    },
    {
      id: "AS1028",
      readable_departure: "11:45",
      readable_arrival: "13:10",
      origin: "EDDH",
      destination: "LFSB",
      departuretime: 42300,
      arrivaltime: 47400
    }
  ];

  describe('should not enable after drop zone', () => {
    test('when drag flight origin matches previous flight destination but its departure is earlier than arrival', () => {
      const _dragFlight = {
        id: 'AS1025',
        readable_departure: '06:20',
        readable_arrival: '07:50',
        origin: 'LFSB',
        destination: 'EDDH',
        departuretime: 22800,
        arrivaltime: 28200
      }
      expect(canIncludesFlightAfter(_dragFlight, rotationFlights[1])).toBeFalsy();
    });

    test('when drag flight origin matches previous flight destination but there is no enough time to turn around', () => {
      const _dragFlight = {
        id: 'AS1025',
        readable_departure: '06:20',
        readable_arrival: '07:50',
        origin: 'LFSB',
        destination: 'EDDH',
        departuretime: 32500,
        arrivaltime: 34900
      }
      expect(canIncludesFlightAfter(_dragFlight, rotationFlights[1])).toBeFalsy();
    });

    test('when drag flight origin does not match previous flight destination', () => {
      const _dragFlight = {
        id: 'AS1025',
        readable_departure: '06:20',
        readable_arrival: '07:50',
        origin: 'XAXA',
        destination: 'EDDH',
        departuretime: 38500,
        arrivaltime: 40900
      }
      expect(canIncludesFlightAfter(_dragFlight, rotationFlights[1])).toBeFalsy();
    });
  });

  describe('should not enable previous drop zone', () => {
    test('when drag flight destination does not match next flight origin', () => {
      const _dragFlight = {
        id: 'AS1025',
        readable_departure: '06:20',
        readable_arrival: '07:50',
        origin: 'LFSB',
        destination: 'EDDH',
        departuretime: 22800,
        arrivaltime: 28200
      }
      expect(canIncludesFlightBefore(_dragFlight, rotationFlights[1])).toBeFalsy();
    });

    test('when drag flight destination matches next flight origin but the arrival time is after next departure', () => {
      const _dragFlight = {
        id: 'AS1025',
        readable_departure: '06:20',
        readable_arrival: '07:50',
        origin: 'EDDH',
        destination: 'LFSB',
        departuretime: 32500,
        arrivaltime: 34900
      }
      expect(canIncludesFlightAfter(_dragFlight, rotationFlights[1])).toBeFalsy();
    });

    test('when drag flight destination matches next flight origin but there is no enough time to turn around', () => {
      const _dragFlight = {
        id: 'AS1025',
        readable_departure: '06:20',
        readable_arrival: '07:50',
        origin: 'EDDH',
        destination: 'LFSB',
        departuretime: 32500,
        arrivaltime: 27000
      }
      expect(canIncludesFlightAfter(_dragFlight, rotationFlights[1])).toBeFalsy();
    });
  });

  describe('should enable after drop zone', () => {
    test('when drag flight origin matches previous flight destination, drag departure time is after previous arrival time and there is enough time to turn around', () => {
      const _dragFlight = {
        id: 'AS1025',
        readable_departure: '06:20',
        readable_arrival: '07:50',
        origin: 'LFSB',
        destination: 'EDDH',
        departuretime: 33300,
        arrivaltime: 34900
      }
      expect(canIncludesFlightAfter(_dragFlight, rotationFlights[1])).toBeTruthy();
    });
  });

  describe('should enable previous drop zone', () => {
    test('when drag flight destination matches previous flight origin, drag arrival time is before previous departure time and there is enough time to turn around', () => {
      const _dragFlight = {
        id: 'AS1025',
        readable_departure: '06:20',
        readable_arrival: '07:50',
        origin: 'LFSB',
        destination: 'LFMN',
        departuretime: 33300,
        arrivaltime: 26700
      }
      expect(canIncludesFlightBefore(_dragFlight, rotationFlights[1])).toBeTruthy();
    });
  });

  describe('rotation timeline', () => {
    test('should return 100% idle if no flights', () => {
      expect(buildScheduleTimelineService([])).toEqual([{ scheduled: SCHEDULES.idle, size: 100 }]);
    });

    test('should return all intermediary turnarounds and idles', () => {
      const intermediary = buildScheduleTimelineService(rotationFlights);
      intermediary.shift();
      intermediary.pop();
      expect(intermediary).toMatchObject([{scheduled: "service", size: 5.208393615666848}, {scheduled: "turnaround", size: 1.3889049641778262}, {scheduled: "idle", size: 0.6944524820889131}, {scheduled: "service", size: 4.861167374622392}, {scheduled: "turnaround", size: 1.3889049641778262}, {scheduled: "idle", size: 2.083357446266739}, {scheduled: "service", size: 6.250072338800218}, {scheduled: "turnaround", size: 1.3889049641778262}, {scheduled: "idle", size: 0.6944524820889131}, {scheduled: "service", size: 5.902846097755761}]);
    });

    test('should return full schedule timeline service', () => {
      expect(buildScheduleTimelineService(rotationFlights)).toEqual([{scheduled: "idle", size: 25.000289355200874}, {scheduled: "service", size: 5.208393615666848}, {scheduled: "turnaround", size: 1.3889049641778262}, {scheduled: "idle", size: 0.6944524820889131}, {scheduled: "service", size: 4.861167374622392}, {scheduled: "turnaround", size: 1.3889049641778262}, {scheduled: "idle", size: 2.083357446266739}, {scheduled: "service", size: 6.250072338800218}, {scheduled: "turnaround", size: 1.3889049641778262}, {scheduled: "idle", size: 0.6944524820889131}, {scheduled: "service", size: 5.902846097755761}, {scheduled: "idle", size: 45.13825391497587,}]);
    });
  });
});
