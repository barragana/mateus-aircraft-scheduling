import flattenDeep from 'lodash.flattendeep';

const DAY_SECONDS = 24 * 60 * 60 - 1;
// 20min in seconds
const MINIMUM_TURN_AROUND_TIME = 1200;
const TURN_AROUND_SIZE = 1200 / DAY_SECONDS * 100;

export const SCHEDULES = {
  service: 'service',
  turnaround: 'turnaround',
  idle: 'idle',
}

export function hasEnoughTurnAroundTime(arrivalFlightTime, departureFlightTime) {
  const interval = departureFlightTime - arrivalFlightTime;
  if (interval < MINIMUM_TURN_AROUND_TIME) return false;
  return true;
}

/**
 * params
 *    flightArrivalTime: Number (flight being dragged arrivaltime)
 */
export function isAircraftOnTheGroundAtMidnight(flightArrivalTime) {
  return flightArrivalTime <= DAY_SECONDS;
}

/**
 * params
 *    destination: String (flight being dragged)
 *    nextOrigin: String (flight in the rotation)
 */
export function destinationMatchesNextFlightOrigin(destination, nextOrigin) {
  return destination === nextOrigin;
}

/**
 * origin: String (flight being dragged)
 * previousDestination: String (flight in the rotation)
 */
 export function originMatchesPreviousFlightDestination(origin, previousDestination) {
  return origin === previousDestination;
}

/**
 * params
 *    flight: Flight (flight being dragged)
 *    rotationFlight: Flight (flight in the rotation)
 */
export function canIncludesFlightBefore(flight, rotationFlight) {
  if (!rotationFlight) return true;
  return (
    isAircraftOnTheGroundAtMidnight(flight.arrivaltime)
    && destinationMatchesNextFlightOrigin(flight.destination, rotationFlight.origin)
    && hasEnoughTurnAroundTime(flight.arrivaltime, rotationFlight.departuretime)
  );
}

/**
 * params
 *    flight: Flight (flight being dragged)
 *    rotationFlight: Flight (flight in the rotation)
 */
export function canIncludesFlightAfter(flight, rotationFlight) {
  if (!rotationFlight) return true;
  return (
    isAircraftOnTheGroundAtMidnight(flight.arrivaltime)
    && originMatchesPreviousFlightDestination(flight.origin, rotationFlight.destination)
    && hasEnoughTurnAroundTime(rotationFlight.arrivaltime, flight.departuretime)
  );
}

/**
 * params
 *    draFlight: Flight (flight being dragged)
 *    previousFlight: Flight (flight in the rotation before the target drop area)
 *    nextFlight: Flight (flight in the rotation after the target drop area)
 */
export function canIncludesFlightIntoSpot(dragFlight, previousFlight, nextFlight) {
  if (previousFlight && !canIncludesFlightAfter(dragFlight, previousFlight)) return false;

  if (nextFlight && !canIncludesFlightBefore(dragFlight, nextFlight)) return false;

  return true;
}


/**
 * Service
 *    scheduled: 'service' | 'turnaround' | 'idle'
 *    size: Number
 *
 * params
 *    rotationFlights: Array<Flight> (all flights in rotation)
 *
 * returns
 *    Array<Service>
 */
export function buildScheduleTimelineService(rotationFlights) {
  if (!rotationFlights?.length) return [{ scheduled: SCHEDULES.idle, size: 100 }];

  const services = rotationFlights.map(({ arrivaltime, departuretime }, i) => {
    const duration = arrivaltime - departuretime;
    const size = duration / DAY_SECONDS * 100;
    const nextFlight = rotationFlights?.[i + 1];
    const partialTimeline = [{ scheduled: SCHEDULES.service, size }];

    if (nextFlight) {
      const interval = nextFlight.departuretime - arrivaltime;
      const idle = interval - MINIMUM_TURN_AROUND_TIME;
      partialTimeline.push({ scheduled: SCHEDULES.turnaround, size: TURN_AROUND_SIZE });
      idle && partialTimeline.push({ scheduled: SCHEDULES.idle, size: (idle / DAY_SECONDS * 100) });
    }
    return partialTimeline;
  });

  const allServices = flattenDeep(services);
  const firstFlight = rotationFlights[0];
  if (firstFlight.departuretime > 0) {
    allServices.unshift({ scheduled: SCHEDULES.idle, size: (firstFlight.departuretime / DAY_SECONDS * 100) })
  }

  const lastFlight = rotationFlights[rotationFlights.length - 1];
  if (lastFlight.arrivaltime < DAY_SECONDS) {
    allServices.push({ scheduled: SCHEDULES.idle, size: (DAY_SECONDS - lastFlight.arrivaltime) / DAY_SECONDS * 100 })
  }
  return allServices;
}

/**
 * params
 *    timeline Array<Service>
 *
 * returns
 *    Number (sum of all service but idle)
 */
export function calculateServiceUsage(timeline) {
  return timeline.reduce((total, { scheduled, size }) => {
    if (scheduled === SCHEDULES.idle) return total;
    return total + size;
  }, 0);
}
