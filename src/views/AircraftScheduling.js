import React from 'react';
import styled from 'styled-components';

import Aircrafts from '../containers/Aircrafts';
import Rotation from '../containers/Rotation';
import Flights from '../containers/Flights';

const Container = styled.div`
  display: grid;
  height: 100%;
  grid-template-areas: "head       head     head"
                       "aircrafts  rotation flights";
  grid-template-columns: minmax(150px, 1fr) 2fr minmax(150px, 1fr);
  grid-template-rows: 4rem 1fr;
  width: 100%;
`;

const Header = styled.h2`
  align-items: center;
  display: flex;
  grid-area: head;
  justify-content: center;
  margin: 0;
`;

const AircraftScheduling = () => (
  <Container>
    <Header>Tomorrow</Header>
    <Aircrafts />
    <Rotation />
    <Flights />
  </Container>
);

export default AircraftScheduling;
