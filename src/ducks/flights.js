export const FETCH_FLIGHTS_REQUESTED = 'FETCH_FLIGHTS_REQUESTED';
export const fetchFlightsRequested = ({ id, offset = 0, limit = 25 } = {}) => ({
  type: FETCH_FLIGHTS_REQUESTED,
  payload: { id, offset, limit },
});

export const FETCH_FLIGHTS_SUCCESSED = 'FETCH_FLIGHTS_SUCCESSED';
export const fetchFlightsSuccessed = ({ pagination, data }) => ({
  type: FETCH_FLIGHTS_SUCCESSED, payload: { pagination, data },
});

export const FETCH_FLIGHTS_FAILED = 'FETCH_FLIGHTS_FAILED';
export const fetchFlightsFailed = error => ({ type: FETCH_FLIGHTS_FAILED, payload: { error } });

/**
 * Flight
 *    id: String
 *    departuretime: Number
 *    arrivaltime: Number
 *    readable_departure: String
 *    readable_arrival: String
 *    origin: String
 *    destination: String
 *
 * request: Object
 *    isFetching: Boolean
 *    didInvalidate: Boolean
 *    error: Error
 * data: Array<Flight>
 * pagination: Object
 *    offset: Number
 *    limit: Number
 *    total: Number
 */
export const initialState = {
  request: {
    isFetching: false,
    didInvalidate: false,
    error: null,
  },
  data: [],
  pagination: null,
};

const stationsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_FLIGHTS_REQUESTED:
      return {
        ...state,
        request: {
          isFetching: true,
          didInvalidate: false,
          error: null,
        }
      };

    case FETCH_FLIGHTS_SUCCESSED:
      return {
        ...state,
        request: {
          isFetching: false,
          didInvalidate: false,
          error: null,
        },
        data: payload.data,
        pagination: payload.pagination,
      };

    case FETCH_FLIGHTS_FAILED:
      return {
        ...state,
        request: {
          isFetching: false,
          didInvalidate: true,
          error: payload.error,
        },
      };

    default:
      return state;
  }
};

export default stationsReducer;
