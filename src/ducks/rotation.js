import { buildScheduleTimelineService } from "../business/rules";

export const ADD_AIRCRAFT = 'ADD_AIRCRAFT';
export const addAircraft = (aircraft) => ({
  type: ADD_AIRCRAFT,
  payload: { aircraft },
});

export const ADD_FLIGHT = 'ADD_FLIGHT';
export const addFlight = (index) => ({
  type: ADD_FLIGHT,
  payload: { index },
});

export const SELECT_FLIGHT = 'SELECT_FLIGHT';
export const selectFlight = (flight) => ({
  type: SELECT_FLIGHT,
  payload: { flight },
});

export const REMOVE_FLIGHT = 'REMOVE_FLIGHT';
export const removeFlight = (flightId) => ({
  type: REMOVE_FLIGHT, payload: { flightId },
});

/**
 * TODO: Rotation flights exchange position
 */

/**
 * flightsHash: { [id: string]: Flight }
 * flights: Array<Flight>
 * aircraft: Aircraft
 * selected: Flight
 */
export const initialState = {
  flights: [],
  aircraft: null,
  flightsHash: {},
  selected: null,
  timeline: buildScheduleTimelineService([]),
};

const stationsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_AIRCRAFT:
      return {
        ...state,
        aircraft: payload.aircraft,
      };

    case ADD_FLIGHT: {
      const nextFlights = [...state.flights]
      nextFlights.splice(payload.index, 0, state.selected);
      return {
        ...state,
        flightsHash: {
          ...state.flightsHash,
          [state.selected.id]: true,
        },
        flights: nextFlights,
        selected: null,
        timeline: buildScheduleTimelineService(nextFlights),
      };
    }

    case SELECT_FLIGHT:
      return {
        ...state,
        selected: payload.flight,
      };

    case REMOVE_FLIGHT: {
      const nextFlights = [...state.flights].filter(({ id }) => id !== payload.flightId);
      const nextState = {
        ...state,
        flights: nextFlights,
        flightsHash: {...state.flightsHash},
        selected: null,
        timeline: buildScheduleTimelineService(nextFlights),
      }
      delete nextState.flightsHash[payload.flightId];
      return nextState;
    }
    default:
      return state;
  }
};

export default stationsReducer;
