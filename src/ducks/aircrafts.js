export const FETCH_AIRCRAFTS_REQUESTED = 'FETCH_AIRCRAFTS_REQUESTED';
export const fetchAircraftsRequested = ({ offset = 0, limit = 25 } = {}) => ({
  type: FETCH_AIRCRAFTS_REQUESTED,
  payload: { offset, limit },
});

export const FETCH_AIRCRAFTS_SUCCESSED = 'FETCH_AIRCRAFTS_SUCCESSED';
export const fetchAircraftsSuccessed = ({ pagination, data }) => ({
  type: FETCH_AIRCRAFTS_SUCCESSED, payload: { pagination, data },
});

export const FETCH_AIRCRAFTS_FAILED = 'FETCH_AIRCRAFTS_FAILED';
export const fetchAircraftsFailed = error => ({ type: FETCH_AIRCRAFTS_FAILED, payload: { error } });

/**
 * Aircraft
 *    ident: String
 *    type: String
 *    economySeats: Number
 *    base: String
 *
 * request: Object
 *    isFetching: Boolean
 *    didInvalidate: Boolean
 *    error: Error
 * data: Array<Aircraft>
 * pagination: Object
 *    offset: Number
 *    limit: Number
 *    total: Number
 */
export const initialState = {
  request: {
    isFetching: false,
    didInvalidate: false,
    error: null,
  },
  data: [],
  pagination: null,
};

const stationsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_AIRCRAFTS_REQUESTED:
      return {
        ...state,
        request: {
          isFetching: true,
          didInvalidate: false,
          error: null,
        }
      };

    case FETCH_AIRCRAFTS_SUCCESSED:
      return {
        ...state,
        request: {
          isFetching: false,
          didInvalidate: false,
          error: null,
        },
        data: payload.data,
        pagination: payload.pagination,
      };

    case FETCH_AIRCRAFTS_FAILED:
      return {
        ...state,
        request: {
          isSubmitting: false,
          didInvalidate: true,
          error: payload.error,
        },
      };

    default:
      return state;
  }
};

export default stationsReducer;
