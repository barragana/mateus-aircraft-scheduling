import { combineReducers } from 'redux';

import aircrafts from './aircrafts';
import flights from './flights';
import rotation from './rotation';

const rootDucks = combineReducers({
  flights,
  aircrafts,
  rotation,
});

export default rootDucks;
