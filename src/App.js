import React from 'react';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';

import theme, { GlobalStyle } from './styles/theme';
import AircraftScheduling from './views/AircraftScheduling';
import store from './store';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Provider store={store}>
        <AircraftScheduling />
      </Provider>
    </ThemeProvider>
  );
}

export default App;
