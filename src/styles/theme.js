import { createGlobalStyle } from 'styled-components';

export const colors = {
  p1: '#800060',
  p7: '#600080',

  n2: '#fafbfc',
  n3: '#bac2cb',
  n4: '#dee1e5',
  n5: '#c1c7cd',
  n6: '#89949e',
  n7: '#4e5f6e',
  n9: '#13293d',
}

export const spacing = {
  xxs: '4px',
  xs: '8px',
  s: '10px',
  m: '12px',
  l: '14px',
  xl: '18px',
  xxl: '24px',
};

export const borderRadius = {
  s: '1px',
  m: '2px',
  l: '4px',
};

export const fontSizes = {
  xs: {
    'font-size': '0.75rem',
    'line-height': '1.25rem',
  },
  s: {
    'font-size': '0.875rem',
    'line-height': '1.5rem',
  },
  m: {
    'font-size': '1rem',
    'line-height': '1.5rem',
  },
  l: {
    'font-size': '1.25rem',
    'line-height': '1.5rem',
  },
  xl: {
    'font-size': '1.5rem',
    'line-height': '2rem',
  }
}

const theme = {
  colors,
  fontSizes,
  spacing,
  borderRadius,
};

export const GlobalStyle = createGlobalStyle`
  html, body, #root {
    height: 100vh;
    width: 100vw;
    overflow-y: hidden;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
      monospace;
  }

  * {
    box-sizing: border-box;
    color: ${({ theme }) => theme.colors.n9};
  }
`;

export default theme;
