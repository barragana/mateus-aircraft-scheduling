
import { css } from 'styled-components';

export const withClickableStyle = ({ disabled, onClick }) =>
  !disabled && onClick && css`
    cursor: pointer;
  `;

export const withFontSize = ({ theme, size }) => theme.fontSizes[size];
