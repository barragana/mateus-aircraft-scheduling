Mateus Schedule Aircraft

### What is include?

- [x] feat: Midnight rule
- [x] feat: Aircraft usage percentage
- [x] feat: Rotation timeline progress bar
- [x] feat: Enforce flight connections
- [x] feat: Adds flight direction into rotation
- [x] feat: Draggable interaction to arrange flights into rotation
- [x] feat: List of aircrafts and flights
- [x] chore: API integration (commented)

### What is not included?

- [ ] feat: Flights pagination with infinite scroll 

### How do I get set up? ###

* `yarn install`
* `yarn start`

### API connect

* API is mocked using real api responses only because CORS is enabled and it was not possible to connect from frontend
* In order to optimize the time to demostrate frontend skills a backend service, to connect to the API, was not created. That would overcome CORS.
* Code was prepared to comunicate directly with the API, it was left commented

### Why & How to run tests

* Test were mainly focused on business rules
* `yarn test`